vim.api.nvim_command('set termguicolors')
vim.api.nvim_command('set background=dark')
vim.api.nvim_command('colorscheme material')
vim.api.nvim_command('let g:material_theme_style = \'darker\'')
vim.api.nvim_command('let g:material_terminal_italics = 1')

vim.o.background = 'dark'
